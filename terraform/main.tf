provider "aws" {
  region = "eu-west-3"
  profile = "aws_josemota"
}

# resource "aws_security_group" "app" {
#   name_prefix = "app-sg-"
# }

resource "aws_security_group" "ec2" {
  name_prefix = "ec2-sg-"
  
  # Allow SSH access for Ansible
  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  # Allow APT to download packages (HTTP)
  egress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }  

  # Allow for docker images to be pulled (HTTPS)
  egress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  # Allow incoming traffic on port 80 (FRONTEND)
  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  # Allow traffic to API
  ingress {
  from_port       = 5000
  to_port         = 5000
  protocol        = "tcp"
  cidr_blocks = ["0.0.0.0/0"]
  }

  # Allow traffic to database
  ingress {
    from_port   = 5432
    to_port     = 5432
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
    # security_groups = [aws_security_group.app.id]
  }
}

variable "instance_names" {
  default = ["db", "back", "front"]
}

resource "aws_instance" "minimal_todo_list_webapp" {
  count           = 3
  ami             = "ami-0f2c91ec8df4bde48"  # AMI ID for Ubuntu 22.04LTS
  instance_type   = "t2.micro"
  key_name        = "aws-ssh-key"
  # security_groups = [aws_security_group.app.name, aws_security_group.ec2.name]
  security_groups = [aws_security_group.ec2.name]
  tags = {
    Name = "${element(var.instance_names, count.index)}"
  }
}

output "instance_public_ip" {
  value = aws_instance.minimal_todo_list_webapp[*].public_ip
}