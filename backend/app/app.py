from flask import Flask, request, jsonify
from flask_cors import CORS
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import Column, Integer, String, Boolean

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql://postgres:postgres@db/todo'
db = SQLAlchemy(app)
CORS(app)

class TodoTask(db.Model):
    __tablename__ = 'tasks'
    id = Column(Integer, primary_key=True)
    text = Column(String, nullable=False)
    completed = Column(Boolean, default=False)

@app.route('/api/tasks', methods=['GET', 'POST'])
def tasks():
    if request.method == 'POST':
        task_text = request.json.get('text')
        if task_text is None:
            return jsonify(error='Task text is missing'), 400

        task = TodoTask(text=task_text)
        db.session.add(task)
        db.session.commit()
        return jsonify(id=task.id, text=task.text, completed=task.completed), 201
    else:
        tasks = db.session.query(TodoTask).all()
        return jsonify([{'id': task.id, 'text': task.text, 'completed': task.completed} for task in tasks])

@app.route('/api/tasks/<int:task_id>', methods=['PUT', 'DELETE'])
def update_task(task_id):
    task = TodoTask.query.get(task_id)
    if task is None:
        return jsonify(error='Task not found'), 404

    if request.method == 'PUT':
        task.completed = not task.completed
    elif request.method == 'DELETE':
        db.session.delete(task)

    db.session.commit()
    return jsonify(id=task.id, text=task.text, completed=task.completed)

if __name__ == "__main__":
    app.run(host="0.0.0.0", port=5000, debug=True) # debug=True for Hot Reload
else:
    application = app 