import unittest
import requests

BASE_URL = 'http://localhost:5000/api/tasks'

class TestToDoApp(unittest.TestCase):

    def test_create_task(self):
        data = {'text': 'Test task'}
        response = requests.post(BASE_URL, json=data)
        self.assertEqual(response.status_code, 201)
        task = response.json()
        self.assertIn('id', task)
        self.assertEqual(task['text'], data['text'])
        self.assertFalse(task['completed'])

    def test_get_all_tasks(self):
        response = requests.get(BASE_URL)
        self.assertEqual(response.status_code, 200)
        tasks = response.json()
        self.assertIsInstance(tasks, list)

    def test_update_task(self):
        data = {'text': 'Test task'}
        response = requests.post(BASE_URL, json=data)
        task_id = response.json()['id']
        data = {'completed': True}
        response = requests.put(BASE_URL + f'/{task_id}', json=data)
        self.assertEqual(response.status_code, 200)
        task = response.json()
        self.assertTrue(task['completed'])

    def test_delete_task(self):
        response = requests.post(BASE_URL, json={'text': 'Task to delete'})
        task_id = response.json()['id']
        response = requests.delete(BASE_URL + f'/{task_id}')
        self.assertEqual(response.status_code, 200)

    def test_invalid_data(self):
        data = {'invalid_field': 'Invalid task data'}
        response = requests.post(BASE_URL, json=data)
        self.assertEqual(response.status_code, 400)
        error_message = response.json().get('error')
        self.assertIsNotNone(error_message)
        self.assertEqual(error_message, 'Task text is missing')

if __name__ == '__main__':
    unittest.main()
