.PHONY: all help docker-build-dev docker-up-dev docker-logs-dev docker-down-dev docker-destroy-dev tests postman docker-build-prod docker-up-prod docker-logs-prod docker-down-prod docker-destroy-prod docker-build-staging docker-up-staging docker-logs-staging docker-down-staging docker-destroy-staging docker-build-datadog docker-up-datadog docker-logs-datadog docker-down-datadog docker-destroy-datadog docker-up-ec2 docker-logs-ec2 docker-down-ec2

all: help

help:
	@echo "Available targets:\n"
	@echo "DEVELOPMENT ENVIRONMENT:"
	@echo "docker-build-dev       : builds/rebuilds the docker images"
	@echo "docker-up-dev          : starts up the app"
	@echo "docker-logs-dev        : shows docker machines logs"
	@echo "docker-down-dev        : shuts down the app"
	@echo "docker-destroy-dev     : shuts down the app, removes docker images and volumes"
	@echo "\n"
	@echo "PRODUCTION ENVIRONMENT :"
	@echo "docker-build-prod      : builds/rebuilds the docker images"
	@echo "docker-up-prod         : starts up the app"
	@echo "docker-logs-prod       : shows docker machines logs"
	@echo "docker-down-prod       : shuts down the app"
	@echo "docker-destroy-prod    : shuts down the app, removes docker images and volumes"
	@echo "docker-tag-prod 		  : tags latest backend production image"
	@echo "\n"
	@echo "TESTING                :"
	@echo "tests                  : runs the backend API tests"
	@echo "postman                : runs Postman collection"
	@echo "\n"
	@echo "MONITORING             :"
	@echo "docker-build-datadog   : builds/rebuilds the docker images"
	@echo "docker-up-datadog      : starts up the app"
	@echo "docker-logs-datadog    : shows docker machines logs"
	@echo "docker-down-datadog    : shuts down the app"
	@echo "docker-destroy-datadog : shuts down the app, removes docker images and volumes"
	@echo "\n"
	@echo "EC2 DEPLOYMENT         :"
	@echo "docker-up-ec2          : sets up ec2 instance"
	@echo "docker-logs-datadog    : shows docker machines logs"
	@echo "docker-down-datadog    : shuts down "


#DEVELOPMENT ENVIRONMENT
docker-build-dev:
	docker compose -f docker-compose.dev.yml build
docker-up-dev:
	docker compose -f docker-compose.dev.yml up -d
docker-logs-dev:
	docker compose -f docker-compose.dev.yml logs -f
docker-down-dev:
	docker compose -f docker-compose.dev.yml down
docker-destroy-dev: docker-down-dev
	docker rmi minimal-todo-list-web-application-backend-dev:latest
	docker rmi postgres:latest
	docker rmi nginx:latest
	docker volume rm minimal-todo-list-web-application_todo-data

#TESTING
tests:
	python3 -m unittest backend/test_app.py
postman:
	postman collection run postman/todo-list.postman_collection.json 

#PRODUCTION ENVIRONMENT
docker-build-prod:
	docker build --tag minimal-todo-list-web-application-backend-prod:latest -f ./backend/Dockerfile.prod ./backend
# docker compose -f docker-compose.prod.yml build
docker-up-prod:
	docker compose -f docker-compose.prod.yml up -d
docker-logs-prod:
	docker compose -f docker-compose.prod.yml logs -f
docker-down-prod:
	docker compose -f docker-compose.prod.yml down
docker-destroy-prod: docker-down-prod
	docker rmi 668042949031.dkr.ecr.eu-west-3.amazonaws.com/todo-repository:latest
	docker rmi postgres:latest
	docker rmi nginx:latest
	docker volume rm minimal-todo-list-web-application_todo-data
	docker rmi minimal-todo-list-web-application-backend-prod:latest
docker-tag-prod:
	docker tag minimal-todo-list-web-application-backend-prod:latest 668042949031.dkr.ecr.eu-west-3.amazonaws.com/todo-repository
docker-push-prod: docker-tag-prod
	docker push 668042949031.dkr.ecr.eu-west-3.amazonaws.com/todo-repository

# STAGING ENVIRONMENT 
# MOCKED FOR SIMPLICITY
docker-build-staging: docker-build-prod
docker-up-staging: docker-up-prod
docker-logs-staging: docker-logs-prod
docker-down-staging: docker-down-prod
docker-destroy-staging: docker-destroy-prod

# MONITORING
#PRODUCTION ENVIRONMENT
docker-build-datadog:
	docker compose -f docker-compose.prod.datadog.yml build
docker-up-datadog:
	docker compose -f docker-compose.prod.datadog.yml up -d
docker-logs-datadog:
	docker compose -f docker-compose.prod.datadog.yml logs -f
docker-down-datadog:
	docker compose -f docker-compose.prod.datadog.yml down
docker-destroy-datadog: docker-down-datadog
	docker rmi minimal-todo-list-web-application-backend-prod:latest
	docker rmi postgres:latest
	docker rmi nginx:latest
	docker rmi datadog/agent:latest
	docker volume rm minimal-todo-list-web-application_todo-data
	docker volume prune